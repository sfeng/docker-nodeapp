var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res) {
	msg = req.headers['x-remote-user'];
  	if (msg == null){
  		msg = "World";
	}

	res.render('index', {
	    title: 'NodeApp Demo',
	    hello: msg + " welcome!",
	    container: process.env.HOSTNAME,
	    ip: process.env.MY_POD_IP,
	    build_number: process.env.BUILD_NUMBER
	});
});

module.exports = router;
