include env.mk

ifndef IMAGE_TAG
	export IMAGE_TAG := latest
endif

.PHONY: all
all: build

.PHONY: build
build: ## build docker image
        @docker buildx create --use
        docker buildx build --platform linux/amd64 \
          -t ${DOCKER_REGISTRY}/${DOCKER_REPO}/${DOCKER_IMAGE}:latest docker

.PHONY: build-push
build-push: ## build docker image
        @docker buildx create --use
        docker buildx build --push --platform linux/amd64 \
          -t ${DOCKER_REGISTRY}/${DOCKER_REPO}/${DOCKER_IMAGE}:latest docker

build:
	docker build -t ${IMAGE} .

build-nc: build
	docker build --no-cache=true -t ${IMAGE} .

prune:
	docker system prune

docker-login:
	cat ${REGISTRY_KEY_FILE} \
		| docker login -u ${REGISTRY_USER} --password-stdin https://${REGISTRY}

push: docker-login
	docker tag ${IMAGE} ${REGISTRY}/${REGISTRY_REPO}/${IMAGE}:${IMAGE_TAG} 
	docker push ${REGISTRY}/${REGISTRY_REPO}/${IMAGE}:${IMAGE_TAG}

pull: docker-login
	docker pull ${REGISTRY}/${REGISTRY_REPO}/${IMAGE}:${IMAGE_TAG}

run:
	docker run -d --name ${IMAGE} -p 8000:8000 ${REGISTRY}/${REGISTRY_REPO}/${IMAGE}:${IMAGE_TAG}

web:
	open http://localhost:8000

teardown:
	docker kill ${IMAGE}; docker rm ${IMAGE}

.PHONY: all build  build_nc prune docker-login push pull run web teardown