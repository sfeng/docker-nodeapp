# Switch back to stable once 3.4 is out
# use edge for GCP
FROM node:8-alpine

RUN apk --update add --no-cache \
	wget

ADD ./demo /demo
RUN cd /demo; npm install

VOLUME /demo

EXPOSE 8000
CMD    ["/demo/bin/www"]
