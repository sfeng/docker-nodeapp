[![Build Status](https://drone.svc.stanford.edu/api/badges/xuwang/docker-nodeapp/status.svg)](https://drone.svc.stanford.edu/xuwang/docker-nodeapp)

# Example NodeJS App in Docker

## Build nodeapp image

### Verify the configuration env defined in env.sh, make sure all the key files are accessible.
    
```
$ make
```

## Push to repo

```
$ make push
```

## CI/CD Support

This repo support [DroneCI](https://drone.med.stanford.edu/xuwang/docker-nodeapp).

To setup the deployment key before git commit and push for ci:

```
$ make sec
```
